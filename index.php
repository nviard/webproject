<?php

use Bridge\Doctrine\EntityManager as EM;

use App\Entity\QRCode as QR;

require 'vendor/autoload.php';

require 'vendor/slim/slim/Slim/Slim.php';

define('ROOT', __DIR__);


\Slim\Slim::registerAutoloader();

$loader = new Twig_Loader_Filesystem('view');
Twig_Autoloader::register();
$twig = new Twig_Environment($loader, array());

$app = new \Slim\Slim(array(
	'view' => new \Slim\Views\Twig()
	));
$app->config = require(__DIR__ . '/app/config/config.php');

$em = new EM($app);
$em = $em->getEntityManager();


$app->get('/', function () use($app,$twig){
	echo $twig->render('index.php');
})->name('home');


$app->run();